//
//  BackSlider.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 09/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import Foundation
import UIKit

class Back
{
    class func backPanel(obj: UIViewController)
    {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            obj.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            obj.view.layoutIfNeeded()
            obj.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            obj.view.removeFromSuperview()
            obj.removeFromParentViewController()
        })
    }
}

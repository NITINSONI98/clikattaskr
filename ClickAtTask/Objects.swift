//
//  Objects.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 09/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import Foundation
import UIKit

class objects{
    var image : UIImage
    var name : String
    
    init(image : UIImage , name : String){
        self.image = image
        self.name = name
    }
}

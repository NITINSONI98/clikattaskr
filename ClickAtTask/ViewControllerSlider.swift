//
//  ViewControllerSlider.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class ViewControllerSlider: UIViewController {
        var viewHeader : ViewHeader?
    var arrayOptions = [[objects(image:#imageLiteral(resourceName: "ic_home_white") ,name:array.home.id),objects(image:#imageLiteral(resourceName: "ic_chat_white"),name:array.liveSupport.id),objects(image:#imageLiteral(resourceName: "ic_shopping_cart_white"),name:array.cart.id),objects(image:#imageLiteral(resourceName: "ic_bookmark_border_white"),name:array.promotions.id),objects(image:#imageLiteral(resourceName: "ic_notifications_none_white"),name:array.notifications.id),objects(image:#imageLiteral(resourceName: "ic_compare_white"),name:array.compareProducts.id)],
                      [objects(image:#imageLiteral(resourceName: "ic_favorite_border_white"),name:array.myFavourites.id),objects(image:#imageLiteral(resourceName: "ic_content_paste_white"),name:array.pendingOrders.id),objects(image:#imageLiteral(resourceName: "ic_schedule_white"),name:array.schedualOrders.id),objects(image:#imageLiteral(resourceName: "ic_star_rate_white_18pt"),name:array.rateMyOredrs.id),objects(image:#imageLiteral(resourceName: "ic_content_paste_white"),name:array.orderHistory.id),objects(image:#imageLiteral(resourceName: "ic_loyalty_white_18pt"),name:array.loyalityPoints.id),objects(image:#imageLiteral(resourceName: "ic_share_white"),name:array.shareApp.id),objects(image:#imageLiteral(resourceName: "ic_content_paste_white"),name:array.termAndConditions.id),objects(image:#imageLiteral(resourceName: "ic_perm_identity_white"),name:array.aboutUs.id),objects(image:#imageLiteral(resourceName: "ic_settings_white"),name:array.setting.id)]]
    @IBOutlet weak var userPicView: UIView!
    @IBOutlet weak var userImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        userPicView.layer.cornerRadius = userPicView.frame.height/2
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.layer.masksToBounds = true
        viewHeader = UINib(nibName: headerName.header.id, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ViewHeader
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Back.backPanel(obj: self)
    }
}
extension ViewControllerSlider: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOptions[section].count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayOptions.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0){
            return 0
        }
        else{
            return 30
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        viewHeader?.lblSectionName.textColor = UIColor.white
        viewHeader?.backgroundColor = UIColor.gray
        viewHeader?.lblSectionName.font = UIFont(name: (viewHeader?.lblSectionName.font.fontName)!, size: 12)
        viewHeader?.lblSectionName.text = headerName.myAccount.id
        viewHeader?.btnViewAll.isHidden = true
            return viewHeader
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell:TableViewCellSlider = tableView.dequeueReusableCell(withIdentifier: tableCells.slider.id, for: indexPath) as? TableViewCellSlider else{ return TableViewCellSlider()}
        cell.Object = arrayOptions[indexPath.section][indexPath.row]
        return cell
   }
}

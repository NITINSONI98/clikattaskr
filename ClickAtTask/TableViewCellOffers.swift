//
//  TableViewCellOffers.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 08/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class TableViewCellOffers: UITableViewCell {
    var section : Int?
    @IBOutlet weak var collectionViewOffers: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionViewOffers.delegate = self
        collectionViewOffers.dataSource = self
        let nibOffers = UINib(nibName: collectionCellsName.offers.id, bundle: nil)
        collectionViewOffers.register(nibOffers, forCellWithReuseIdentifier: collectionCells.offers.id)
                let nibRecommended = UINib(nibName: collectionCellsName.recommended.id, bundle: nil)
        collectionViewOffers.register(nibRecommended, forCellWithReuseIdentifier: collectionCells.recommended.id)
    }
}
extension TableViewCellOffers: UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 3
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if(section == 2){
        guard let cell1:CollectionViewCellOffers = (collectionView.dequeueReusableCell(withReuseIdentifier: collectionCells.offers.id, for: indexPath) as? CollectionViewCellOffers) else{return CollectionViewCellOffers()}
        cell1.layer.cornerRadius = 4
        return cell1
        }
        else{
        guard let cell1:CollectionViewCellRecommended = (collectionView.dequeueReusableCell(withReuseIdentifier: collectionCells.recommended.id, for: indexPath) as? CollectionViewCellRecommended) else{return CollectionViewCellRecommended()}
            cell1.layer.cornerRadius = 4
            return cell1
        }
    }
}

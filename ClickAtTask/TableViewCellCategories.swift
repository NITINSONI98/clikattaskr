//
//  TableViewCellCategories.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 08/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//
import UIKit
import Kingfisher

class TableViewCellCategories: UITableViewCell {
    var numberOfCategoriesCell : Int?
    var imageUrl : [String]?
    var categoriesName : [String]?
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionViewCategories.delegate = self
        collectionViewCategories.dataSource = self
        let nib = UINib(nibName: collectionCellsName.categories.id, bundle: nil)
        collectionViewCategories.register(nib, forCellWithReuseIdentifier: collectionCells.categories.id)
    }
}

extension TableViewCellCategories: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
     return categoriesName!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell1:CollectionViewCellCategories = (collectionView.dequeueReusableCell(withReuseIdentifier: collectionCells.categories.id , for: indexPath) as? CollectionViewCellCategories) else{return CollectionViewCellCategories()}
        cell1.lblName.text = categoriesName?[indexPath.row]
        let url = URL(string: (imageUrl?[indexPath.row])!)
        cell1.imgImage.kf.setImage(with: url)
        cell1.layer.cornerRadius = 4
        return cell1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if(indexPath.row == (categoriesName?.count)!-1){
        if((categoriesName?.count)! % 2 == 0){
            return CGSize.init(width: 140, height: 150)
        }
        else{
            return CGSize.init(width: 300, height: 150)
        }
        }
        else{
            return CGSize.init(width: 140, height: 150)
        }
    }
}

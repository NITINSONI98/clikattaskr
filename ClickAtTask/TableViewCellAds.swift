//
//  TableViewCellAds.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 08/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class TableViewCellAds: UITableViewCell {

    @IBOutlet weak var collectionViewAds: UICollectionView!
    @IBOutlet weak var viewSearchBar: UIView!
    @IBOutlet weak var viewContent: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionViewAds.delegate = self
        collectionViewAds.dataSource = self
        let nib = UINib(nibName: collectionCellsName.ads.id, bundle: nil)
        collectionViewAds.register(nib, forCellWithReuseIdentifier: collectionCells.ads.id)
        viewSearchBar.layer.cornerRadius = 4
        viewSearchBar.layer.masksToBounds = true
         Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(pageSlide), userInfo: nil, repeats: true)
    }
    func pageSlide(){
        
        let pageWidth:CGFloat = self.collectionViewAds.frame.width
        let maxWidth:CGFloat = pageWidth * 5
        let contentOffset:CGFloat = self.collectionViewAds.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth
        {
            slideToX = 0
        }
        self.collectionViewAds.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.collectionViewAds.frame.height), animated: true)
    }
}
extension TableViewCellAds: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let myWidth = self.viewContent.frame.width
        let myHeight = self.viewContent.frame.height
        return CGSize(width: myWidth, height:myHeight)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell1:CollectionViewCellAds = (collectionView.dequeueReusableCell(withReuseIdentifier: collectionCells.ads.id, for: indexPath) as? CollectionViewCellAds) else{return CollectionViewCellAds()}
        cell1.layer.cornerRadius = 4
        return cell1
    }
}

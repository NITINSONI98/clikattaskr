//
//  ViewHeader.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 09/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class ViewHeader: UIView {
    @IBOutlet weak var lblSectionName: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!    
}

//
//  Constants.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 10/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import Foundation
enum array : String{
    case home = "Home"
    case liveSupport = "Live support"
    case cart = "Cart"
    case promotions = "Promotions"
    case notifications = "Notifications"
    case compareProducts = "Compare Products"
    case myFavourites = "My favourites"
    case pendingOrders = "Pending Oredrs"
    case schedualOrders = "Scheduled Orders"
    case rateMyOredrs = "Rate my oredr"
    case orderHistory = "Order history"
    case loyalityPoints = "Loyality points"
    case shareApp = "Share app"
    case termAndConditions = "Terms & Conditions"
    case aboutUs = "About Us"
    case setting = "Setings"
    var id: String{
        return self.rawValue
    }
}

enum tableCells:String {
    case ads = "identifierTableViewCellAds"
    case categories = "identifierTableViewCellCategories"
    case offers = "identifierTableViewCellOffers"
    case slider = "identifierTableCellSlider"
    var id:String {
        return self.rawValue
    }
}


enum collectionCells:String {
    case ads = "identifierCollectionViewCellAds"
    case categories = "identifierCollectionViewCellCategories"
    case offers = "identifierCollectionViewCellOffers"
    case recommended = "identifierCollectionViewCellRecommended"
    var id : String {
        return self.rawValue
    }
}
enum collectionCellsName:String{
    case ads = "CollectionViewCellAds"
    case categories = "CollectionViewCellCategories"
    case offers = "CollectionViewCellOffers"
    case recommended = "CollectionViewCellRecommended"
    var id: String{
       return self.rawValue
    }
}
enum error:String{
    case errorOccured = "error occured"
    var id:String{
        return self.rawValue
    }
}
enum tableCellsName:String{
    case ads = "TableViewCellAds"
    case categories = "TableViewCellCategories"
    case offers = "TableViewCellOffers"
    var id: String{
        return self.rawValue
    }
}
enum headerName: String{
    case offers = "Offers"
    case recommended = "Recommended"
    case myAccount = "My Account"
    case header = "ViewHeader"
    var id : String{
        return self.rawValue
    }
}
enum slider : String{
    case sliderIdentifier = "identifierOptionSlider"
    var id : String{
        return self.rawValue
    }
}
enum parameter:String{
    case keyAreaId = "areaId"
    case keyCountryId = "201"
    case valueAreaId = "countryId"
    case valueCountryId = "1"
    var id :String{
        return self.rawValue
    }
}
enum apiUrl : String{
    case url = "https://appbean.clikat.com/get_all_category"
    var id : String{
        return self.rawValue
    }
}

//
//  TableViewCellSlider.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 09/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit

class TableViewCellSlider: UITableViewCell {
    
    @IBOutlet weak var lblOptions: UILabel!
    @IBOutlet weak var imgImageOptions: UIImageView!
    
        var Object : objects? {
            didSet{
                updateUI()
            }
        }
    
    fileprivate func updateUI(){
         imgImageOptions?.image = Object?.image
         lblOptions.text = Object?.name
        }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

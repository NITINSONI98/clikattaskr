//
//  ViewController.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import SVProgressHUD

class ViewController: UIViewController {
    var numberOfCategoriesCell : Int = 0
    var heightOfCategoriesCell = 158
    var viewHeader : ViewHeader?
    var recievedData : Instructor?
    var imageUrl : [String] = []
    var catagoriesName : [String] = []
    @IBOutlet weak var tableViewMain: UITableView!
    @IBOutlet weak var btnOptions: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibAds = UINib(nibName: tableCellsName.ads.id, bundle: nil)
        tableViewMain.register(nibAds, forCellReuseIdentifier: tableCells.ads.id)
        
        let nibCategories = UINib(nibName: tableCellsName.categories.id, bundle: nil)
        tableViewMain.register(nibCategories, forCellReuseIdentifier: tableCells.categories.id)
        
        let nibOffers = UINib(nibName: tableCellsName.offers.id, bundle: nil)
        tableViewMain.register(nibOffers, forCellReuseIdentifier: tableCells.offers.id)
        sendData()
    }
    
    func sendData() {
        SVProgressHUD.show()
        let param:[String:Any] = [parameter.keyAreaId.id:parameter.valueAreaId.id,parameter.keyCountryId.id:parameter.valueCountryId.id]
        ApiHit.fetchData(urlStr: apiUrl.url.id, parameters: param) { (jsonData) in
            self.recievedData = Mapper<Instructor>().map(JSONObject: jsonData)
            self.numberOfCategoriesCell = (self.recievedData?.data?.english?.count)!
            self.recievedData!.data!.english?.enumerated().forEach{ index,newData in
            self.imageUrl.append((newData.image)!)
            self.catagoriesName.append((newData.name)!)
            self.tableViewMain.reloadData()
            SVProgressHUD.dismiss()
            }
        }
    }
    @IBAction func btnOptions(_ sender: UIButton) {
        Appearance.ShowPanel(obj: self, identifier: slider.sliderIdentifier.id)
    }
    }
extension ViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 2 || section == 3){
        return 50
        }
    else{
        return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        viewHeader = UINib(nibName: headerName.header.id, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? ViewHeader

        if (section == 2){
            
        viewHeader?.lblSectionName.text = headerName.offers.id
        viewHeader?.btnViewAll.isHidden = false
          return viewHeader
        }else{
        viewHeader?.lblSectionName.text = headerName.recommended.id
        viewHeader?.btnViewAll.isHidden = true
        return viewHeader
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section
        {
        case 1:
            let variable = (numberOfCategoriesCell/2 + numberOfCategoriesCell % 2)
            let heightOfCategories = (variable * heightOfCategoriesCell)
            return CGFloat(heightOfCategories)
        default:
            tableViewMain.estimatedRowHeight = 200
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        switch indexPath.section {
        case 0:
             guard let cellAds:TableViewCellAds = tableView.dequeueReusableCell(withIdentifier: tableCells.ads.id , for: indexPath) as? TableViewCellAds else{ return TableViewCellAds()}
             return cellAds
        case 1:
             guard let cellCategories:TableViewCellCategories = tableView.dequeueReusableCell(withIdentifier: tableCells.categories.id, for: indexPath) as? TableViewCellCategories else{ return TableViewCellCategories()}
             cellCategories.imageUrl = self.imageUrl
             cellCategories.categoriesName = self.catagoriesName
             cellCategories.numberOfCategoriesCell = recievedData?.data?.english?.count
             cellCategories.collectionViewCategories.reloadData()
             return cellCategories
        case 2,3:
            guard let cellOffers:TableViewCellOffers = tableView.dequeueReusableCell(withIdentifier: tableCells.offers.id, for: indexPath) as? TableViewCellOffers else{ return TableViewCellOffers()}
            cellOffers.section = indexPath.section
            return cellOffers
        default:
             guard let cellCategories:TableViewCellCategories = tableView.dequeueReusableCell(withIdentifier: tableCells.categories.id, for: indexPath) as? TableViewCellCategories else{ return TableViewCellCategories()}
             return cellCategories
        }
    }
}

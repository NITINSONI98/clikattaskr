//
//  Appear.swift
//  ClickAtTask
//
//  Created by Sierra 4 on 07/03/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import Foundation
import UIKit

class Appearance
{
    class func ShowPanel(obj: UIViewController ,identifier: String)
    {
        let OptionViewController : UIViewController = obj.storyboard!.instantiateViewController(withIdentifier: identifier)
        obj.view.addSubview(OptionViewController.view)
        obj.addChildViewController(OptionViewController)
        OptionViewController.view.layoutIfNeeded()
        OptionViewController.view.frame = CGRect(x: -UIScreen.main.bounds.size.width , y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            OptionViewController.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        })
    }
}
